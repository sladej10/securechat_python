#!flask/Scripts/python
import pymysql
import logging
from model import User
from random import randint
from flask import url_for
from flask.ext.login import current_user
import security
import os

dbconfig = os.path.join(os.path.dirname(__file__), 'dbconfig') 
dbvars = {}
with open(dbconfig) as rawfile: 
  f = rawfile.read().splitlines() #Strip newlines
  for line in f:
    if line.startswith('#'): #Skip comment lines
      continue
    (key, val) = line.split('=')
    dbvars[key] = val
    logging.warn('Loaded: '+key+", "+val)
db = pymysql.connect(host=dbvars['DB_HOSTNAME'], user=dbvars['DB_USERNAME'], password=dbvars['DB_PASSWORD'], db=dbvars['DB_DBNAME'])
db.autocommit(True)
  
def userExists(email):
  cur = db.cursor()
  cmd = "select count(*) from user where email=%s"
  cur.execute(cmd, (email,))
  results = cur.fetchone()
  result = True if results[0] == 1 else False
  logging.warn("cur.fetchone(): "+str(results[0]))
  logging.warn("rowcount: "+str(cur.rowcount))
  cur.close() 
  return result 
  
def userForEmail(email):
  cur = db.cursor()
  cmd = "select user_id, email, firstname, lastname, phone from user where email=%s"
  cur.execute(cmd, (email,))   
  result = ''
  if cur.rowcount == 0:
    return None
  else:
    results = cur.fetchone()
    return User(results[0], results[1], results[2], results[3], results[4])

def profileImageNameForUserId(user_id):
  cur = db.cursor()
  cmd = "select filename from user_profile_photo where user_id = %s"
  cur.execute(cmd, (str(user_id)+'%',))
  result = ''
  if cur.rowcount == 0:
    return None
  else:
    results = cur.fetchone()
    return str(user_id)+"_"+results[0]
    
def userForId(user_id):
  cur = db.cursor()
  cmd = "select user_id, email, firstname, lastname, phone from user where user_id=%s"
  cur.execute(cmd, (user_id,))   
  result = ''
  if cur.rowcount == 0:
    return None
  else:
    results = cur.fetchone()
    return User(results[0], results[1], results[2], results[3], results[4])
  
def firstnameForEmail(email):
  fullnameDict = fullnameDictForEmail(email)
  if not fullnameDict:
    return None
  return fullnameDict['firstname']  
  
def fetchoneclose(cursor):
  if cursor.rowcount == 0:
    result = None
  else:
    result = cursor.fetchone()[0]
  return result

def sessionCookieFor(email):
  cur = db.cursor()  
  cmd = "select cookie from session_cookie inner join user on(session_cookie.email = user.email) where user.email = %s"  
  cur.execute(cmd, (email, ))
  return fetchoneclose(cur)
  
def deleteSessionCookie(email):
  cur = db.cursor()  
  cmd = "delete from session_cookie where email=%s"  
  cur.execute(cmd, (email,))      
  
def saveSessionCookie(cookie, email):
  #delete old
  deleteSessionCookie(email)
  cur = db.cursor()  
  cmd = "insert into session_cookie (email, cookie) values (%s, %s)"  
  cur.execute(cmd, (email, cookie))
  cur.close()

def getPasswordForEmail(email):
  cur = db.cursor()  
  cmd = "select password from user where email = %s"
  cur.execute(cmd, (email,))
  return fetchoneclose(cur);
  
def getPasswordForId(id):
  cur = db.cursor()  
  cmd = "select password from user where user_id = %s"
  cur.execute(cmd, (id,))
  return fetchoneclose(cur);
  
  #for row in cur.fetchall():
  #  t+=str(row[0])+", "+row[1]
  #db.close()

def insertProfilePhoto(user_id, timestamp):
  logging.warn("Inserting photo for user id: "+str(user_id))
  cur = db.cursor()
  cmd = "insert into user_profile_photo (user_id, filename) values (%s, %s)"
  cur.execute(cmd, (user_id, str(timestamp)))
  cur.close()

def deleteOldProfilePhotos(user_id):
  cur = db.cursor()
  cmd = "delete from user_profile_photo where user_id=%s"
  cur.execute(cmd, (user_id,))
  cur.close()

def insertUser(email, firstname, lastname, phone, password):
  cur = db.cursor()
  cmd = "insert into user (email, firstname, lastname, phone, password) values (%s, %s, %s, %s, %s)"
  cur.execute(cmd, (email, firstname, lastname, phone, password))
  cur.close()    
  
def updateUser(email, firstname, lastname, phone, password):
  id = current_user.user_id
  user = userForId(id)
  
  #If password not given, fetch the current
  if not password:                         
    password = getPasswordForId(user.user_id)
  else: 
    password = security.hash(password, email)
  cur = db.cursor()
  cmd = "update user set email=%s, firstname=%s , lastname=%s, phone=%s, password=%s where user_id=%s"
  
  cur.execute(cmd, (email, firstname, lastname, phone, password, id))
  cur.close()    

def setPassword(email, password):
  cur = db.cursor()
  password = security.hash(password, email)
  cmd = "update user set password=%s where email=%s"
  cur.execute(cmd, (password, email))
  cur.close()    

LOGIN_ATTEMPTS_QUERY = "select attempts from login_attempt inner join user on (user.user_id = login_attempt.user_id) where user.email=%s and ip_addr=%s"
ANON_LOGIN_ATTEMPTS_QUERY = "select attempts from login_attempt inner join user on (user.user_id = login_attempt.user_id) where ip_addr=%s"
  
def get_login_attempts(client_ip, email=None):
  cur = db.cursor()
  if not email:
    cmd = ANON_LOGIN_ATTEMPTS_QUERY
    logging.warn("anon, "+str(client_ip)+", "+str(email))
    cur.execute(cmd, (client_ip))
  else:
    cmd = LOGIN_ATTEMPTS_QUERY
    logging.warn("non anon, "+str(client_ip)+", "+str(email))
    cur.execute(cmd, (email, client_ip))  
  result = fetchoneclose(cur)
  logging.warn("Result: "+str(result))
  #Returns None if no attempts yet
  return result
  
def incr_login_attempts(client_ip, email):
  #Get count
  user = userForEmail(email)
  if not user:
    logging.warn("User to increment attempts not found!")
    return 0 #This should not happen
  user_id = user.user_id
  
  attempts = get_login_attempts(client_ip, email)
  logging.warn("attempts: "+str(attempts))     
  #Increment
  if not attempts:
    cur2 = db.cursor()
    logging.warn("Inserting attempt record")
    cmd2 = "insert into login_attempt(user_id, attempts, ip_addr) values (%s, %s, %s)"
    cur2.execute(cmd2, (user_id, "1", client_ip))
  else:
    cur2 = db.cursor()
    cmd2 = "update login_attempt set attempts=attempts+1 where user_id = %s and ip_addr = %s"
    cur2.execute(cmd2, (user_id, client_ip))
  return (attempts + 1) if attempts else 1

def delete_login_attempts(email, client_ip):
    user = userForEmail(email)
    if not user:
      return #This should not happen
    user_id = user.user_id
    cur = db.cursor()
    cmd = "delete from login_attempt where user_id = %s and ip_addr = %s"
    cur.execute(cmd, (user_id, client_ip))

def set_login_attempts(email, client_ip, attempts):
    user = userForEmail(email)
    if not user:
      return #This should not happen
    user_id = user.user_id
    cur = db.cursor()
    cmd = "update login_attempt set attempts=%s where user_id = %s and ip_addr = %s"
    cur.execute(cmd, (attempts, user_id, client_ip))
  

