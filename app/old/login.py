#!flask/Scripts/python
from passlib.hash import pbkdf2_sha256
import db
import security
import sys
import ui
from flask import render_template
from flask import make_response
from prefill import emailInput

login_manager = LoginManager()

def init(app):
  login_manager.init_app(app)
  
@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)

def userExists(email):
  return db.userExists(email)

def pwMatches(email, password):
  hash = db.getPassword(email)
  if hash == None:
    return False
  result = security.verifyPw(password, hash)
  return result
  
def handle(email, password):
  if userExists(email) == False: 
    return ui.login(emailErr='User does not exist', email=email)
  if pwMatches(email, password) == False:
    return ui.login(pwErr = 'Invalid password', email=email)
  user = db.userForEmail(email)
  firstname = user['firstname']
  response = make_response(render_template('dashboard.html', firstname = firstname if firstname else None))
  response.set_cookie('email', email, max_age=1200, expires=None, path='/', secure=False)
  response.set_cookie('firstname', firstname, max_age=1200, expires=None, path='/', secure=False)
  sessionCookie = security.sessionCookie(email, user['id'], security.hash(password))
  db.saveSessionCookie(sessionCookie, email)
  response.set_cookie('session', sessionCookie, max_age=1200, expires=None, path='/', secure=False)
  return response
  
def check(sessionCookie, email):
  if sessionCookie is None:
    return False
  if email is None:
    return False
  pwHash = db.getPassword(email)
  if pwHash is None:
    return False
  return security.verifySessionCookie(sessionCookie, pwHash, email)
  
  

