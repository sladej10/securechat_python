#!flask/Scripts/python
from flask import render_template
from prefill import emailInput
from prefill import pwInput
from flask.ext.login import login_required

def login(form, captcha='', captcha_path='', email='', captchaErr='', emailErr='', pwErr='', barMsg=''):
  return render_template('login.html', form = form, captcha = captcha, captcha_path=captcha_path, captchaErr=captchaErr, emailErr=emailErr, pwErr=pwErr, barMsg = barMsg)
  

@login_required
def dashboard(form, profilePhotoErr=''):
  return render_template('dashboard.html', profilePhotoErr = profilePhotoErr, form = form)
  
  

