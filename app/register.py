#!flask/Scripts/python
from wtforms import Form, BooleanField, TextField, PasswordField, validators
from baseform import BaseForm
from passlib.hash import pbkdf2_sha256
import validate
from login import userExists
from flask import render_template, url_for, abort, redirect
from prefill import formInput
from prefill import emailInput
from security import hash
from security import encrypt
import logging
import email
import re
import db

class RegistrationForm(BaseForm):
    firstname = TextField('First name')#, [validators.Length(min=2, max=25)])
    lastname = TextField('Last name')#, [validators.Length(min=2, max=25)])
    email = TextField('Email')#, [validators.Length(min=6, max=40)])
    phone = TextField('Phone')#, [validators.Length(min=11, max=13)])
    password = PasswordField('Password')#, [
        #validators.Required(), validators.Length(min=8, max=100),
        #validators.EqualTo('confirm', message='Passwords do not match')
    #])
    confirm = PasswordField('Confirm password')
    #accept_tos = BooleanField('I agree with the terms and conditions', [validators.Required()])

    def validate_on_submit():
      return True
      
def register(email, password):
  hash = hash(password, email)
  db.insertUser(email, hash)
  
def handle(form, email, firstname, lastname, phone, password, confirmPassword):
  errors = validate.check_user(email, firstname, lastname, phone, password, confirmPassword)
  #If everything OK, insert new user to db
  if validate.isOk(errors):
    if not form.validate(): ##Our data valid but form invalid -> CSRF attempt
        logging.warn(str("Register errors: "+str(form.errors)))
        abort(400)
    db.insertUser(email, firstname, lastname, phone, hash(password, email))
    #email.send('info@safechat.com', email, '')
    #return redirect('login.html', form = form, barMsg="Check your inbox for a confirmation link to complete your registration") 
    return render_template('reg_successful.html')#, barMsg="Registration successful. You can now log in"))#"Check your inbox for a confirmation link to complete your registration"))
    
  #Otherwise go back with errors and prefilled values 
  return render_template('register.html', 
      form = form,
      emailErr = errors['emailErr'], 
      firstnameErr = errors['firstnameErr'], 
      lastnameErr = errors['lastnameErr'], 
      phoneErr = errors['phoneErr'],
      pwErrs = errors['pwErrs'], 
      confPwErr = errors['confPwErr']
      )
 


