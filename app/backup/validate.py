#!flask/Scripts/python
from passlib.hash import pbkdf2_sha256
import re
  
def pw(password):
  lengthErr = len(password) < 8
  noDigitErr = re.search(r"\d", password) is None
  noLowercaseErr = re.search(r"[a-z]", password) is None 
  noUppercaseErr = re.search(r"[A-Z]", password) is None
  if not (lengthErr or noDigitErr or noLowercaseErr or noUppercaseErr):
    return None
  return {
    'lengthErr' : 'must have at least 8 letters' if lengthErr else '',
    'noDigitErr' : 'must contain at least one digit' if noDigitErr else '',
    'noLowercaseErr' : 'must contain a lower case letter' if noLowercaseErr else '',
    'noUppercaseErr' : 'must contain an upper case letter' if noUppercaseErr else ''}
    
def phone(password):
  return re.match(r"\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4}", password)
  
def email(email):
  return re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email)
