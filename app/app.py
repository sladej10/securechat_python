#!flask/Scripts/python
import os
import os.path
import login
import register
import security
import db        
import pymysql
import ctypes
import dashboard
import validate
from upload import UploadImageForm, upload_profile_photo, get_profile_photo_for
from flask import render_template, url_for, request
from flask import redirect, abort, Flask, session

import ui
from prefill import formInput
from prefill import emailInput
from prefill import pwInput
from register import RegistrationForm
from login import LoginForm
from flask.ext.login import login_user, logout_user, login_required, current_user
import logging

app = Flask(__name__)
app.config.from_object('config')
app.debug = True #DELETE BEFORE DEPLOYMENT
if __name__ == '__main__':
  app.run()    

#app = Flask(__name__, static_url_path='/static', static_folder='static_test')
root = os.path.dirname(os.path.abspath(__file__))
login.init(app)



@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('site_login'))

@app.route('/', methods=['GET', 'POST'])
def site_login():           
  if current_user.is_authenticated:
    return site_dashboard_get()
    
  form = LoginForm(request.form)
  if request.method == 'POST':
    if form.validate_on_submit:
      session['remember_me'] = form.remember_me.data
      
      email = form.email.data
      captchaErr, captcha, captcha_web_path, captcha_os_path = '', '', '', ''
      password = form.password.data
      user_exists = login.userExists(email)
      
      #Delete previous captcha file
      if form.captcha.data:
        security.delete_captcha(form.captcha_expected.data)
      #Delete captcha files older than 10 min
      security.delete_old_captchas()
      
      #Check password  
      if not email or user_exists == False:
        return ui.login(form, emailErr = 'Unknown e-mail')
      
      #Check captcha, save error before we override vars
      if form.captcha_expected.data and form.captcha.data.lower() != form.captcha_expected.data.lower():
        captchaErr = 'Wrong captcha'
      
      #Check attempts
      client_ip = request.environ['REMOTE_ADDR']
      if user_exists: 
        attempts = login.incr_attempts(client_ip, email)
        if attempts >= 5:
          captcha, captcha_web_path, captcha_os_path = security.captcha()
          form.captcha_expected.data = captcha
      
      #Reset captcha field
      form.captcha.data=''
      
      #If was error, kick user out
      if captchaErr:
        return ui.login(form, captcha, captcha_web_path, captchaErr = 'Wrong captcha')
         
      if not password or login.pwMatches(email, password) == False:
        return ui.login(form, captcha, captcha_web_path, pwErr = 'Invalid password')
      session['user_id'] = form.email
      
      
      # --- All checks passed ---
      if not form.validate(): #Our data valid but form invalid -> CSRF attempt
        logging.warn(str("Login errors: "+str(form.errors)))
        abort(400)
        
      # Reset captcha attempts
      login.delete_attempts(email, client_ip)
      
      #Log user in with remember me, if present
      user = db.userForEmail(form.email.data)
      remember_me = False
      if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
      login_user(user, remember = remember_me)
      logging.warn(str("Login errors: "+str(form.errors)))
      next = request.args.get('next')
      #if not next_is_valid(next):
      #  return abort(400)
      #return redirect(next or url_for('site_dashboard'))
      
      
      return redirect(url_for('site_dashboard_get'))
  elif request.method == 'GET':
    #Check attempts
    email = form.email.data
    #Delete captcha files older than 10 min
    security.delete_old_captchas()
                                                      
    captcha, captcha_web_path, captcha_os_path = '', '', ''
    client_ip = request.environ['REMOTE_ADDR']
    attempts = login.get_attempts(client_ip) 
    if attempts and attempts >= 5:
      captcha, captcha_web_path, captcha_os_path = security.captcha()
      form.captcha_expected.data = captcha
    return ui.login(form, captcha, captcha_web_path)

@app.route('/register', methods=['GET', 'POST'])
def site_register():
    form = RegistrationForm(request.form)
    
    if request.method == 'POST': 
        return register.handle(form, form.email.data, form.firstname.data, form.lastname.data,form.phone.data, form.password.data, form.confirm.data)
    return render_template('register.html', form = form)
  
@app.route('/', methods=['POST'])
def handleLogin():
  #TODO num attempts check
  return login.handle(request.form['email'], request.form['password'])
  
@app.route('/dashboard', methods=['GET'])
@login_required
def site_dashboard_get():
    u = current_user
    form = dashboard.EditForm(request.form)
    dashboard.setupProfileForm(form, email=u.email, firstname=u.firstname, lastname=u.lastname, phone=u.phone)
    formerrors = {}
    return render_template('dashboard.html', photo_path=get_profile_photo_for(u.user_id), form = form, formerrors = formerrors) 

@app.route('/dashboard', methods=['POST']) 
@login_required
def site_dashboard_post(): 
    email, firstname, lastname, phone, oldPassword, newPassword = request.form['email'], request.form['firstname'], request.form['lastname'], request.form['phone'], request.form['oldPassword'], request.form['newPassword']
    form = dashboard.EditForm(request.form)
    u = current_user
    formerrors = validate.check_user(email, firstname, lastname, phone)
    #Extra values that validate doesn't set
    formerrors['newPwErrs'] = {}
    formerrors['oldPwErr'] = ''
    
    #We check passwords separately, so ignore them in errors from check_user
    if oldPassword or newPassword:
      if not oldPassword:
        formerrors['oldPwErr'] = 'Enter old password'  
      else:
        #Old password is set 
        if security.verifyPw(oldPassword, email, db.getPasswordForId(current_user.user_id)):
          #Allow password change if old pw matches
          formerrors['newPwErrs'] = validate.check_pw(newPassword) if newPassword else {'1' : "Enter new password"}
        else:  
          formerrors['oldPwErr'] = 'Old password is incorrect'
          
    #If its current user's email, ignore that it's same in DB
    if u.email == email:
      formerrors['emailErr'] = ''
    #Ignore validation.py error (pw was not filled, we use our own pw errors here)
    formerrors['pwErrs'] = {}
        
    #If profile photo, upload it
    photoFile = request.files['profilePhoto']
    if photoFile:
      path, upload_error = upload_profile_photo(photoFile, u.user_id)
      formerrors['profilePhotoErr'] = upload_error
    ok = validate.isOk(formerrors)
    if ok and (len(formerrors['newPwErrs']) == 0) and not formerrors['oldPwErr']:
      if not form.validate(): #Our data valid but form invalid -> CSRF attempt
        logging.warn(str("Dashboard errors: "+str(form.errors)))
        abort(400) 
      db.updateUser(email, firstname, lastname, phone, newPassword)
      u = current_user #Reload user
      msg = 'Changes successfully saved'
    else:
      msg = 'Please correct errors'
    logging.warn(str(formerrors))
    dashboard.setupProfileForm(form, email=email, firstname=firstname, lastname=lastname, phone=phone)
    return render_template('dashboard.html', form = form, photo_path=get_profile_photo_for(u.user_id), formerrors = formerrors, newPwErrs = formerrors['newPwErrs'], msg = msg) #redirect(url_for('site_dashboard_get'))
    
'''@app.route('/register', methods=['GET'])
def registerPage():
  return render_template('register.html',
      emailInput = emailInput(''),
      firstnameInput = formInput('text', 'firstname', 'form-control', 'First name', '', 'required'), 
      lastnameInput = formInput('text', 'lastname', 'form-control', 'Last name', '', 'required'),
      phoneInput = formInput('tel', 'phone', 'form-control', 'Phone number', '', ''))
'''

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
    
@app.errorhandler(401)
def page_not_found(e):
    return render_template('401.html'), 401       
    
@app.errorhandler(400)
def page_not_found(e):
    return render_template('400.html'), 400    
  
@app.route('/register', methods=['POST'])
def handleRegister():
  return register.handle(
      request.form['email'], 
      request.form['firstname'], 
      request.form['lastname'], 
      request.form['phone'], 
      request.form['password'],
      request.form['confirm-password'])




