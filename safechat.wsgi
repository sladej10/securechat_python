import os, sys

PROJECT_DIR = '/home/daniel/sites/safechat/app'

activate_this = os.path.join(PROJECT_DIR, 'flask/Scripts', 'activate_this.py')
exec(open(activate_this).read(), dict(__file__=activate_this))
sys.path.insert(0, PROJECT_DIR)

from app import app as application
